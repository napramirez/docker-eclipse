FROM openjdk:8-jdk

ARG ECLIPSE_URL=https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2020-03/R/eclipse-java-2020-03-R-linux-gtk-x86_64.tar.gz&r=1
ARG LOMBOK_URL=https://repo1.maven.org/maven2/org/projectlombok/lombok/1.18.12/lombok-1.18.12.jar

RUN apt update && apt install -y libswt-gtk-4-java

RUN mkdir -p /usr/share/eclipse
RUN curl -sLo - $ECLIPSE_URL | tar zxv -C /usr/share/eclipse --strip-components=1
RUN curl -s $LOMBOK_URL -o /usr/share/eclipse/lombok.jar
RUN echo '-javaagent:/usr/share/eclipse/lombok.jar' >> /usr/share/eclipse/eclipse.ini

RUN useradd -m eclipseuser -s /bin/bash
USER eclipseuser
WORKDIR /home/eclipseuser

CMD ["/usr/share/eclipse/eclipse"]
