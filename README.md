# Dockerized Eclipse IDE

> Note: For now, this must be built on your own machines until the dependencies are sorted out. This image depends on `napramirez/maven-user`.

## How to Run
```
docker run -ti --rm -e DISPLAY=:0 -v /tmp/.X11-unix:/tmp/.X11-unix -v "$PWD:/home/eclipseuser/app" -v m2-user-home:/home/eclipseuser/.m2 docker-eclipse
```

